import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { View, Content, Card, Text, Button, Form, Item, Input, Label } from 'native-base'
import Header from '../../components/Header'
// import Input from '../components/Input'
// import Card from '../components/CardComponent'
import CardComponent from '../../components/CardComponent'


export default class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
}
    render() {
        return (
            <>
                <Header title={'Login'} />
                <Content>
                    <View style={styles.mainView}>
                        <View style={styles.topMargin} />
                        <View style={styles.topView}>
                            <View style={styles.upperMainView}>
                                <Text style={styles.largeText}>
                                    Log In
                                </Text>
                            </View>
                            <View style={styles.subView}>
                                <Text style={styles.secondaryText}>
                                    Login using your Email
                                </Text>
                            </View>
                        </View>
                        <Form>
                            <View style={styles.cardSection}>
                                <CardComponent props={this.props}>
                                        <View style={{ flex: 1, flexDirection: 'row', height: 60, alignItems: 'center' }}>
                                            <View style={{ flex: 0.5, borderBottomColor: "#050849", borderBottomWidth: 2 }}>
                                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'center', marginVertical: 10 }}>
                                                    <Text style={{ fontWeight: '600', fontSize: 18 }}>
                                                        Email Address
                                                  </Text>
                                                </View>
                                            </View>
                                        </View>
                                    <View style={{ paddingHorizontal: 20 }}>
                                        <View>
                                            <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16 }}>Email Address</Label>
                                            <Item regular>
                                                <Input style={{ height: 40 }} />
                                            </Item>
                                        </View>
                                        <View>
                                            <Label style={{ marginBottom: 5, marginTop: 15, fontSize: 16 }}>Password</Label>
                                            <Item regular>
                                                <Input style={{ height: 40 }} type={'password'} secureTextEntry={true}/>
                                            </Item>
                                         </View>
                                         <View style={{ marginTop: 20 }}>
                                                <Button style={{ backgroundColor: "#050849", height: 50 }} full dark>
                                                    <Text style={{ fontWeight: '600' }}>Log In</Text>
                                                </Button>
                                        </View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                                                <Text style={{ fontSize: 12 }}>
                                                    Forgot Password
                                                </Text>
                                                <Text style={{ fontSize: 12 }}>
                                                    Dont Have Account? Register
                                                </Text>
                                            </View>
                                    </View>
                                </CardComponent>
                            </View>
                        </Form>
                    </View>
                </Content>
            </>
        )
    }
}


const styles = StyleSheet.create({
    topMargin: {
        marginTop: 45
    },
    topView: {
        width: '100%',

        marginTop: 10,
        marginBottom: 20,

    },
    mainView: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    upperMainView: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    largeText: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 20
    },
    secondaryText: {
        fontSize: 14,
        color: 'gray'
    },
    subView: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    cardSection: {
        flexDirection: 'row',
        width: '95%',
        justifyContent: 'center'
    },
    card: {
        flex: 1,
        paddingBottom: 30,
        paddingTop: 10,
        // elevation: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 5,
        width: '100%',
        // marginRight:5
    }
})