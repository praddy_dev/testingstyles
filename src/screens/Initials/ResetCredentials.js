import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { View, Content, Card, Text, Button, Form, Item, Input, Label } from 'native-base'
import Header from '../../components/Header'
// import Input from '../components/Input'
// import Card from '../components/CardComponent'
import CardComponent from '../../components/CardComponent'


export default class ResetCredentials extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <>
                <Header title={'ChangePassword'} />
                <Content>
                    <View style={styles.mainView}>
                        <View style={styles.topMargin} />
                        <View style={styles.topView}>
                            {/* <View style={styles.upperMainView}>
                                <Text style={styles.largeText}>
                                    Log In
                                </Text>
                            </View>
                            <View style={styles.subView}>
                                <Text style={styles.secondaryText}>
                                    Login using your Email
                                </Text>
                            </View> */}
                        </View>
                        <Form>
                            <View style={styles.cardSection}>
                                <CardComponent props={this.props}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginVertical: 20 }}>
                                        {/* <View style={{  justifyContent: 'center',  marginVertical: 10,borderColor:'red',borderWidth:1 }}> */}

                                        <Text style={styles.largeText}>
                                            Reset Login Password
                                </Text>
                                        {/* </View> */}
                                    </View>
                                    <View style={{ marginHorizontal: 25, borderWidth: 1, borderRadius: 2, borderColor: '#677eff', backgroundColor: '#677eff', }}>
                                        <View style={{paddingHorizontal:20,paddingVertical:15}}>
                                            <Text style={{ color: 'white', fontWeight: 'bold', textAlign: 'center' }}>For Security Purpose no withdrawls are permitted for 24 hour after modification of security method.</Text>
                                  </View>
                                        </View>
                                    <View style={{ paddingHorizontal: 40 }}>
                                     
                                        <View>
                                            <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16 }}>Email or Phone number</Label>
                                            <Item regular>
                                                <Input style={{ height: 40, maxHeight: 40 }} />
                                            </Item>
                                        </View>
                                        <View style={{ marginTop: 20, marginBottom: 10 }}>
                                            <Button style={{ backgroundColor: "#050849", height: 60, maxHeight: 50 }} full dark>
                                                <Text style={{ fontWeight: '600' }}>Submit</Text>
                                            </Button>
                                        </View>

                                    </View>
                                </CardComponent>
                            </View>
                        </Form>
                    </View>
                </Content>
            </>
        )
    }
}


const styles = StyleSheet.create({
    topMargin: {
        marginTop: 45
    },
    topView: {
        width: '100%',

        marginTop: 10,
        marginBottom: 20,

    },
    mainView: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    upperMainView: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    largeText: {
        color: '#000',
        fontWeight: '400',
        fontSize: 20,
        textAlign: 'center'
    },
    secondaryText: {
        fontSize: 14,
        color: 'gray'
    },
    subView: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    cardSection: {
        flexDirection: 'row',
        width: '95%',
        justifyContent: 'center'
    },
    card: {
        flex: 1,
        paddingBottom: 30,
        paddingTop: 10,
        // elevation: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 5,
        width: '100%',
        // marginRight:5
    }
})