import React, { Component } from 'react'
import { StyleSheet, ScrollView } from 'react-native'
import { View, Content, Card, Text, Button, Form, Item, Input, Label,Icon, Container } from 'native-base'

import Header from '../../components/Header'
import CardComponent from '../../components/CardComponent'
import Fontisto from 'react-native-vector-icons/Fontisto'
// import Input from '../components/Input'



export default class Register extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <>
                    <Header title={'Wallet'} />
                
                {/* <View style={{flex:1,height:'100%'}}> */}
                <ScrollView style={{ paddingTop: 40,backgroundColor:'white' }}>
                    <Container>
                    <Content>
                            <View style={styles.mainView}>
                                {/* <View style={styles.topMargin} /> */}
                                <View style={styles.topView}>
                                    <View style={styles.upperMainView}>

                                        <Text style={styles.largeText}>
                                            Create Your Account</Text>
                                    </View>
                                    <View style={styles.subView}>
                                        <Text style={styles.secondaryText}> Create Your Account</Text>
                                        <Text style={styles.secondaryText}>by filling below details</Text>

                                    </View>
                            </View>

                                <Form>
                                    <View style={styles.cardSection}>
                                        <CardComponent cardStyle={{paddingBottom:15}}>
                                            <View style={{ paddingHorizontal: 20 }}>
                                                <View>
                                                    <Label style={{ marginBottom: 5, marginTop: 20, fontSize: 16 }}>First Name</Label>
                                                    <Item regular>
                                                        <Input style={{ maxHeight: 40 }} />
                                                    </Item>
                                                </View>
                                                <View>
                                                    <Label style={{ marginBottom: 5, marginTop: 15, fontSize: 16 }}>Last Name</Label>
                                                    <Item regular>
                                                        <Input style={{ maxHeight: 40 }} />
                                                    </Item>
                                                </View>

                                                <View>
                                                    <Label style={{ marginBottom: 5, marginTop: 15, fontSize: 16 }}>Email</Label>
                                                    <Item regular>
                                                        <Input style={{ maxHeight: 40 }} />
                                                    </Item>
                                                </View>

                                                <View>
                                                    <Label style={{ marginBottom: 5, marginTop: 15, fontSize: 16 }}>Date of Birth</Label>
                                                    <Item regular>
                                                        <Input style={{ maxHeight: 40 }} />
                                                    </Item>
                                                </View>
                                                <View>
                                                    <Label style={{ marginBottom: 5, marginTop: 15, fontSize: 16 }}>Address</Label>
                                                    <Item regular>
                                                        <Input style={{ maxHeight: 40 }} />
                                                    </Item>
                                                </View>
                                                <View>
                                                    <Label style={{ marginBottom: 5, marginTop: 15, fontSize: 16 }}>City</Label>
                                                    <Item regular>
                                                        <Input style={{ maxHeight: 40 }} />
                                                    </Item>
                                                </View>


                                                <View style={{justifyContent:'center',flexDirection:'row'}}>
                                                    <View style={{paddingTop:10}}><Fontisto name={'angle-dobule-down'} color='blue' size={22} /></View>
                                                </View>
                                            </View>
                                        </CardComponent>
                                    </View>
                                </Form>
                            </View>
                    </Content>
                       </Container>
                        </ScrollView>
            </>
        )
    }
}


const styles = StyleSheet.create({
    topMargin: {
        marginTop: 45
    },
    topView: {
        width: '100%',

        marginTop: 10,
        marginBottom: 20,

    },
    mainView: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    upperMainView: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    largeText: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 20
    },
    secondaryText: {
        fontSize: 14,
        color: 'gray'
    },
    subView: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    cardSection: {
        flexDirection: 'row',
        width: '95%',
        justifyContent: 'center'
    },
    card: {
        flex: 1,
        paddingBottom: 30,
        paddingTop: 10,
        // elevation: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 5,
        width: '100%',
        // marginRight:5
    }
})