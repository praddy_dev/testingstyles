import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { View, Content, Card, Text, Button ,Form } from 'native-base'
import Header from '../components/Header'
import Input from '../components/Input'


export default class WalletScreen extends Component {


    render() {
        return (
            <>
                <Header title={'Wallet'} />
                <Content>
                    <View style={styles.mainView}>
                        <View style={styles.topMargin} />
                        <View style={styles.topView}>
                        <View style={styles.mainText}>
                                <Text
                                    style={{
                                        color: '#000',
                                        fontWeight: 'bold',
                                        fontSize: 20
                                    }}>
                                    Log In</Text>
                            </View>
                            <View style={styles.subText}>
                                <Text style={{fontSize:14,color:'gray'}}>Login using your Email</Text>
                            </View>
                        </View>
                        <Form>
                        <View style={styles.cardSection}>
                                <Card style={styles.card} >
                                        <View style={{ flex: 1, flexDirection: 'row', height: 60, alignItems: 'center' }}>
                                            <View style={{ flex: 0.5, borderBottomColor: "#050849", borderBottomWidth: 2 }}>
                                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'center', marginVertical: 10 }}>
                                                    <Text style={{ fontWeight: '600', fontSize: 18 }}>
                                                        Email Address
                        </Text>
                                                </View>
                                            </View>

                                        </View>                 
                                    <View style={{ paddingHorizontal: 20 }}>
                                        <Input title='Email Address' type='email' />
                                        <Input title='Password' type='password' />
                                        
                                            <View style={{ marginTop: 20 }}>
                                                <Button style={{ backgroundColor: "#050849", height: 50 }} full dark>
                                                    <Text style={{ fontWeight: '600' }}>Log In</Text>
                                                </Button>

                                            </View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                                                <Text style={{ fontSize: 12 }}>
                                                    Forgot Password
                            </Text>
                                                <Text style={{ fontSize: 12 }}>
                                                    Dont Have Account? Register
                            </Text>
                                            </View>
                                  
                                    </View>
                                </Card>

                            </View>
                        </Form>
                        </View>
                    </Content>
            </>
        )
    }
}


const styles = StyleSheet.create({
    topMargin: {
      marginTop:45  
    },
    topView: {
       width:'100%',
        
        marginTop: 10,
        marginBottom: 20,
       
    },
    mainView: {
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    mainText: {
        justifyContent: 'center',
        alignItems: 'center',
      
    },
    subText: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:10
    },
    cardSection: {
        flexDirection:'row',
        width: '95%',
        justifyContent:'center'
    },
    card: {
        flex: 1,
        paddingBottom: 30,
        paddingTop: 10,
        // elevation: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 5,
        width: '100%',
        // marginRight:5
    }
})