import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { View, Text, Content, Container, Form } from 'native-base'
// import Card from '../components/basicCard'
import Header from '../components/Header'



export default class HistoryScreen extends Component {


    render() {
        return (
            <>
                <Header title={'Wallet'} />
                
            </>
        )
    }
}


const styles = StyleSheet.create({
    topMargin: {
        marginTop: 45
    },
    topView: {
        width: '100%',

        marginTop: 10,
        marginBottom: 20,

    },
    mainView: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    mainText: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    subText: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    cardSection: {
        flexDirection: 'row',
        width: '95%',
        justifyContent: 'center'
    }
})