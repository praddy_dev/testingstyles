import React, { Component } from 'react'
import { StyleSheet, ScrollView } from 'react-native'
import { View, Text, Content, Container, Form } from 'native-base'
import Header from '../components/Header'
import CardComponent from '../components/CardComponent'
import Input from '../components/Input'



export default class ExchangeScreen extends Component {
    constructor(props) {
    super(props)
}

    render() {
        return (
            <>
            <Container>
                    <Header title={'Wallet'} />
                    <ScrollView>

                <Content>
                    <View style={styles.mainView}>
                        <View style={styles.topMargin} />
                                <View style={styles.topView}>
                                    <View style={styles.upperMainView}>

                                        <Text style={style.largeText}>
                                    Create Your Account</Text>
                            </View>
                                    <View style={styles.subView}>
                                        <Text style={styles.secondaryText}> Create Your Account</Text>
                                        <Text style={styles.secondaryText}>by filling below details</Text>

                            </View>
                        </View>
                        <Form>
                                    <View style={styles.cardSection}>
                                        <CardComponent props={this.props}>
                                            <View style={{ paddingHorizontal: 20 }}>
                                    <Input title='Name' type='text' />
                                    <Input title='Name' type='text' />

                                    <Input title='Name' type='text' />

                                    <Input title='Name' type='text' />

                                                <Input title='Name' type='text' />
                                                <Input title='Name' type='text' />
                                                </View>
                                    </CardComponent>
                            </View>
                        </Form>
                    </View>
                    </Content>
                    </ScrollView>
                 </Container>
            </>
        )
    }
}


const styles = StyleSheet.create({
    topMargin: {
        marginTop: 45
    },
    topView: {
        width: '100%',

        marginTop: 10,
        marginBottom: 20,

    },
    mainView: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    upperMainView: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    largeText: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 20
    },
    secondaryText: {
        fontSize: 14,
        color: 'gray'
    },
    subView: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    cardSection: {
        flexDirection: 'row',
        width: '95%',
        justifyContent: 'center'
    },
    card: {
        flex: 1,
        paddingBottom: 30,
        paddingTop: 10,
        // elevation: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 5,
        width: '100%',
        // marginRight:5
    }
})