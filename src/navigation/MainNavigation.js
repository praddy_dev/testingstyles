import React, { useState } from "react";
import Login from "../screens/Initials/Login";
import Register from "../screens/Initials/Register";
import ChangePassword from "../screens/Initials/ChangePassword";
import ResetCredentials from "../screens/Initials/ResetCredentials";

import MoreRoute from "../screens/MoreScreen";
import EvilIcons from 'react-native-vector-icons/EvilIcons'

import { BottomNavigation } from "react-native-paper";

const Screen = () => {

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: "Login", title: "Login", icon: "message", color: "#3F51B5" },
        { key: "Register", title: "Register", icon: "album", color: "#3F51B5" },
        { key: "ChangePassword", title: "History", icon: "album", color: "#3F51B5" },
        { key: "ResetCredentials", title: "Reset", icon: "history", color: "#3F51B5" },

    ]);
    const iconDef = () => <EvilIcons name={'chart'} />
    const renderScene = BottomNavigation.SceneMap({
        Login: Login,
        Register: Register,
        ChangePassword: ChangePassword,
        ResetCredentials: ResetCredentials
    });

    return (
        <BottomNavigation
            navigationState={{ index, routes }}
            onIndexChange={setIndex}
            renderScene={renderScene}
        />
    );
};

export default Screen;
