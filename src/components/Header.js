import * as React from 'react';
import { Image, View } from 'react-native'
import{Icon} from 'native-base'
import { Appbar, } from 'react-native-paper';

const MyComponent = (props) => {
    console.log('Went back',props);
    const _goBack = () => console.log('Went back');

    const _handleSearch = () => console.log('Searching');

    const _handleMore = () => console.log('Shown more');

    return (
        <Appbar.Header style={{minHeight:80}}>
            <View style={{flexDirection:'row',paddingHorizontal:20}}>
                <View style={{
                    flex: 8,
                width: 40, height: 40
            }}>
            <Image source={require('../../assets/img/logo-white-salamtech.png')}
             resizeMode={'contain'}  />
                </View>
                <View style={{flex:3,justifyContent:'center',alignItems:'flex-end'}}>
                    <Icon name={'options'} style={{color:'#28a745'}}/>
                    </View>
           
                </View>
        </Appbar.Header>
    );
};
export default MyComponent;
