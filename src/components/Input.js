import React from 'react'
import { StyleSheet } from 'react-native'
import { Container, Header, View, Content, Card, CardItem, Text, Body, Form, Item, Input, Label } from 'native-base';

const InputDef = (props) => {
    return (
        <View>

            <Label style={{marginBottom:5,marginTop:20,fontSize:16}}>{props.title}</Label>
            <Item regular>
                <Input style={{height:40}} />
            </Item>
            
            </View>
    )
}

const styles = StyleSheet.create({
    inputView: {
        borderWidth: 1,
        borderColor: '#ccc',
        width: '100%',
    }
})   

export default InputDef;