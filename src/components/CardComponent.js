import * as React from 'react';
import { StyleSheet } from 'react-native'
import { Card } from 'native-base';

const CardComponent = (props) => {
    return (
        <Card style={[styles.card,props.cardStyle]} >
            {props.children}
        </Card>
    )
}

    const styles = StyleSheet.create({
        card: {
            flex: 1,
            paddingBottom: 30,
            paddingTop: 10,
            borderWidth: 1,
            borderRadius: 2,
            borderColor: '#ddd',
            borderBottomWidth: 0,
            shadowColor: '#ccc',
            shadowOffset: { width: 0, height: 4 },
            shadowOpacity: 0.8,
            shadowRadius: 4,
            elevation: 5,
            width: '100%',
        }
    })
    export default CardComponent;