import React from "react";
import { configureFonts, DefaultTheme } from "react-native-paper";
import customFonts from "./Fonts";

const theme = {
    ...DefaultTheme,
    fonts: configureFonts(customFonts),
    colors: {
        ...DefaultTheme.colors,
        primary: "#050849",
        secondory:'#74dfcf',
        accent: "#74dfcf",
        text:'#212529'
    },
};

export default theme;